'use strict';

var config = {
    rootFolder: '../',
    templateFolder: 'assets/templates/',
    outputFolder: 'assets/test/service/',
    deferredActions: [
        'brew services restart varnish4',
    ],
    site: {
        hostname: 'spree.dev',
        url: 'https://spree.dev',
        ip: '127.0.0.1',
        services: {
            account: {
                orderItemsPerPage: 6,
                clusterCount: 2,
            },
            assets: {
                cacheLife: 60 * 60 * 24 //1 day
            }
        }
    },
    mysql: {
        host: 'localhost',
        database: 'spree_148403283',
        username: 'root',
        password: 'root',
        port: '3600'
    },
    rabbit: {
        enabled: false,
        host: 'localhost',
        port: 5672,
        username: 'spree',
        password: 'r4bb1t',
        vhost: '/'
    },
    redis: {
        enabled: true,
        host: 'localhost',
        port: 6379,
        password: '',
        timeout: 2.5,
        persistent: '',
        db: 0,
    },
    elasticsearch: {
        host: 'localhost',
        port: '9200'
    }
}

module.exports = config;
