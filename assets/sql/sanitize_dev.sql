-- gtm
UPDATE `core_config_data` SET `value` = '' WHERE `path` = 'google/analytics/account';
UPDATE `core_config_data` SET `value` = 'GTM-NWLDZH' WHERE `path` = 'spree_gtm/tag_manager/account';
-- URL
UPDATE `core_config_data` SET `value` = 'https://spree.dev/' WHERE `path` in ('web/unsecure/base_url','web/secure/base_url');

-- No password expiry
UPDATE `core_config_data` SET `value` = NULL WHERE `path` = 'admin/security/password_lifetime';

-- No url secure key in admin
UPDATE `core_config_data` SET `value` = NULL WHERE `path` = 'admin/security/use_form_key';


LOCK TABLES `admin_role` WRITE , `admin_user` WRITE;

SET @SALT = "autouser";
SET @PASS = CONCAT(MD5(CONCAT( @SALT , ".") ), CONCAT(":", @SALT ));
SET @FIRSTNAME = "Mark";
SET @SURNAME="Holtzhausen";
SET @EMAIL= "nemesarial+markh_spree@gmail.com";
SET @USERNAME="markh";

SELECT @EXTRA := MAX(extra) FROM admin_user WHERE extra IS NOT NULL;

INSERT INTO `admin_user` (firstname,lastname,email,username,password,created,lognum,reload_acl_flag,is_active,extra,rp_token_created_at) 
VALUES (@FIRSTNAME,@SURNAME,@EMAIL,@USERNAME,@PASS,NOW(),0,0,1,@EXTRA,NOW());

INSERT INTO `admin_role` (parent_id,tree_level,sort_order,role_type,user_id,role_name) 
VALUES (1,2,0,'U',(SELECT user_id FROM admin_user WHERE username = @USERNAME),@FIRSTNAME);

UNLOCK TABLES;