<cmake outputFile="account/conf/env.js" updateAction="s.restart account" />
'use strict'
/** Configured By Mark H */
var env = require(__dirname + '/env.dev.js')();
const deepAssign = require('deep-assign');
// const mod_bunyan = require('bunyan');


module.exports = function() {
    let devEnv=deepAssign({},env,{
        api:{
            host: '{{&site.url}}'
        },
        site:{
            pageSize: {{site.services.account.orderItemsPerPage}},
            mosaic:{
                urlPrefix: 'https://spree.co.za'
            }
        },
        cluster:{
            count: {{site.services.account.clusterCount}}
        }

    });
    
    // devEnv.logging.streams.push({level : mod_bunyan.DEBUG, stream: process.stdout});

    return devEnv;
};
