<cmake outputFile="assets/conf/env.js" updateAction="s.restart assets" />
'use strict';
module.exports = (function () {

    var env = require(__dirname + '/env.dev.js');

	env.http.cache = {
        assets: 'public,max-age=31536000'
    };
    env.http.surrogate={
                assets: 'max-age={{site.services.assets.cacheLife}}', // 1 day + 1 day
                footer: 'max-age={{site.services.assets.cacheLife}}', // 365 days + 7 days
                menu: 'max-age={{site.services.assets.cacheLife}}', // 365 days + 7 days
                header: 'max-age={{site.services.assets.cacheLife}}', // 365 days + 7 days
                barcode: 'max-age={{site.services.assets.cacheLife}}' // 365 days + 7 days
    }
    env.cluster.count=1;


    return env;

}());
