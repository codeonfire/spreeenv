<cmake outputFile="varnishcache/conf/backends.vcl"/>
vcl 4.0;

backend catalog_sitemaps {
    .host = "{{site.ip}}";
    .port = "3000";
    .max_connections = 50;
}

backend mage1 {
    .host = "{{site.ip}}";
    .port = "8081";
    .max_connections = 50;
}

backend mage2 {
    .host = "{{site.ip}}";
    .port = "8081";
    .max_connections = 50;
}

backend mage3 {
    .host = "{{site.ip}}";
    .port = "8081";
    .max_connections = 50;
}

backend mage4 {
    .host = "{{site.ip}}";
    .port = "8081";
    .max_connections = 50;
}

# backend mage5 {
#     .host = "{{site.ip}}";
#     .port = "8081";
#     .max_connections = 50;
# }

backend mage6 {
    .host = "{{site.ip}}";
    .port = "8081";
    .max_connections = 50;
}

backend mage_api1 {
    .host = "{{site.ip}}";
    .port = "8085";
    .max_connections = 50;
}

backend mage_api2 {
    .host = "{{site.ip}}";
    .port = "8085";
    .max_connections = 50;
}

backend mage_api3 {
    .host = "{{site.ip}}";
    .port = "8085";
    .max_connections = 50;
}

backend mage_api4 {
    .host = "{{site.ip}}";
    .port = "8085";
    .max_connections = 50;
}

backend mage_api6 {
    .host = "{{site.ip}}";
    .port = "8085";
    .max_connections = 50;
}

backend newsletters1 {
    .host = "{{site.ip}}";
    .port = "3010";
    .max_connections = 50;
}

backend newsletters2 {
    .host = "{{site.ip}}";
    .port = "3010";
    .max_connections = 50;
}

backend s3 {
    .host = "kraken-lossless.s3-website-eu-west-1.amazonaws.com";
    .port = "80";
    .max_connections = 100;
}

backend content {
    .host = "{{site.ip}}";
    .port = "8083";
    .max_connections = 50;
}

backend catalog1 {
    .host = "{{site.ip}}";
    .port = "3000";
    .max_connections = 50;
}

backend catalog2 {
    .host = "{{site.ip}}";
    .port = "3000";
}

backend apig1 {
    .host = "{{site.ip}}";
    .port = "10010";
    .max_connections = 50;
}

backend apig2 {
    .host = "{{site.ip}}";
    .port = "10010";
    .max_connections = 50;
}

backend checkout1 {
    .host = "{{site.ip}}";
    .port = "3300";
    .max_connections = 50;
}

backend checkout2 {
    .host = "{{site.ip}}";
    .port = "3300";
}

backend shipping_areas1 {
    .host = "{{site.ip}}";
    .port = "3005";
    .max_connections = 50;
}

backend shipping_areas2 {
    .host = "{{site.ip}}";
    .port = "3005";
    .max_connections = 50;
}

backend assets1 {
    .host = "{{site.ip}}";
    .port = "3200";
    .max_connections = 50;
}

backend assets2 {
    .host = "{{site.ip}}";
    .port = "3200";
    .max_connections = 50;
}

backend catalog_feeds {
    .host = "{{site.ip}}";
    .port = "3111";
    .max_connections = 10;
}


backend account1 {
    .host = "{{site.ip}}";
    .port = "4100";
    .max_connections = 100;
}

backend account2 {
    .host = "{{site.ip}}";
    .port = "4100";
    .max_connections = 100;
}


backend thread {
    .host = "{{site.ip}}";
    .port = "8090";
    .max_connections = 10;
}

acl purgers {
    "{{site.ip}}";
    "localhost";
}

