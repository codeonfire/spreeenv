<cmake outputFile="catalog/conf/env.js" updateAction="s.restart catalog"/>
var env = require(__dirname + '/env.dev.js')();

module.exports = function() {

    env.cluster.count = 2;
    env.cluster.verbose = true;

    env.elasticsearch.client.hosts = ['{{elasticsearch.host}}:{{elasticsearch.port}}'];
    env.elasticsearch.client.deadTimeout = 3000;
    env.elasticsearch.client.maxRetries = 5;

    env.redis.client.host = "{{redis.host}}";
    env.ga.profile_id = 'UA-39442395-1';

    env.media.host = "";

    env.http.surrogate.category_view = 'content="ESI/1.0", max-age=30+10';
    env.http.surrogate.serp = 'content="ESI/1.0", max-age=30+10';
    env.http.surrogate.more_like_this = 'max-age=30+10';
    env.http.surrogate.product_detail = 'content="ESI/1.0", max-age=30+10';
    env.http.surrogate.brand_pg = 'content="ESI/1.0", max-age=30+10';
    env.http.surrogate.brands_list = 'content="ESI/1.0", max-age=0+0';
    env.http.surrogate.history_product = 'content="ESI/1.0", max-age=0+0';

    env.cluster.count=1;


    return env;
};
