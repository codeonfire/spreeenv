<cmake outputFile="checkout/conf/env.js" updateAction="s.restart checkout" />
var env = require(__dirname + '/env.dev.js')(); 

module.exports = function() {
    'use strict';

    // Peach Config
    env.peach.port = 443;
    env.peach.host = 'test.oppwa.com';
    env.peach.authentication.userId = '8a8294174d2e4980014d45caab56209e';
    env.peach.authentication.password = '6wXbyJWy';
    env.peach.authentication.entityId = '8a8294174fbc3f9e014fd618d5a42b07';
    env.peach.form_action_host = "https://spree.dev/";
    env.peach.redirectUrl = "https://spree.dev/en/peachapi/standard/response";


    // Checkout Config
    env.api.host = "{{&site.url}}";
    env.domain = "{{&site.url}}";
    env.gtm.id="GTM-5953XX";

    env.cluster.count=1;

    return env;
};
