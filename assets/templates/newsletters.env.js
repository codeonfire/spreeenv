<cmake outputFile="newsletters/conf/env.js" updateAction="s.restart newsletters" />
'use strict';
var env = require(__dirname + '/env.defaults.js')();

module.exports = function() {

    env.cluster.count   = 1;
    env.cluster.verbose = true;
    env.retries         = 10;
    env.host            = "http://develop.spreeza.net/";

    env.mysql = {
        masters: [{
            host: "db.dev.spreeza.net",
            port: "3306",
            user: "newsletter",
            password: "97sU32D9Y7ny",
            database: "newsletter",
        }],
        slaves: [{
            host: "db.dev.spreeza.net",
            port: "3306",
            user: "newsletter",
            password: "97sU32D9Y7ny",
            database: "newsletter",
        }]
    };

    env.magento = {
        api: {
            username: 'Newsletters_Api',
            api_key: '6X6TnbtCkgL5',
            endpoint: 'http://develop.spreeza.net/api/?wsdl'
        }
    };

    env.emarsys.data_store.directory   = "/srv/newsletters-develop/var/lib/emarsys/";
    env.emarsys.logs.directory         = "/srv/newsletters-develop/var/log/emarsys/";

    env.mailchimp.data_store.directory = '/srv/newsletters-develop/var/lib/mailchimp/';
    env.mailchimp.logs.directory       = '/srv/newsletters-develop/var/log/mailchimp/';

    return env;
};
