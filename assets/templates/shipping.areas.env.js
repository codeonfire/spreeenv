<cmake outputFile="shipping.areas/conf/env.js" updateAction="s.restart shipping.areas" />
'use strict';
var env = require(__dirname + '/env.defaults.js')();

module.exports = function() {

    env.cluster.verbose = true;

    env.elasticsearch.hosts = ['{{elasticsearch.host}}:{{elasticsearch.port}}'];
    env.elasticsearch.replicas = 0;
    env.elasticsearch.indexer.replication = "sync";

    env.mysql.host = "{{mysql.host}}";
    env.mysql.user = "{{mysql.username}}";
    env.mysql.password = "{{mysql.password}}";
    env.mysql.database = "{{mysql.database}}";
    env.mysql.port = '{{mysql.port}}';

    env.logging.src = true;

    env.http.cache.suggest = 'public,max-age=10,s-max-age=10,must-revalidate';
    env.http.surrogate.suggest = 'max-age=30';

    return env;
};
