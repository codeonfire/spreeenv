const express = require('express')
const app = express()
const inspect = require('debug')('inspect');

app.get('*', function(req, res) {
    inspect(req)
    res.send('Hello World!')
})

app.listen(3123, function() {
    console.log('Example app listening on port 3123!')
})
