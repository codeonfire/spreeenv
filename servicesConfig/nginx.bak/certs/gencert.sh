#!/bin/bash
openssl req -newkey rsa:2048 -x509 -nodes -keyout nginx.key -new -out nginx.crt -subj /CN=spree.dev -reqexts SAN -extensions SAN -config <(cat /System/Library/OpenSSL/openssl.cnf <(printf '[SAN]\nsubjectAltName=DNS:spree.dev')) -sha256 -days 3650
