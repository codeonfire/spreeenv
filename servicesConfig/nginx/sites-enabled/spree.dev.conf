## ---------------------------
## Define Varnish
## ---------------------------
upstream varnish {
  server 127.0.0.1:6081;
  keepalive 2536;
}

upstream spreecoza {
    server www.spree.co.za:443;
    keepalive 4;
}

## ---------------------------
## Main Incoming on Port 80
## ---------------------------
server {
	rewrite_log on;

	listen 80 default;
	server_name spree.dev www.spree.dev;

#    set $redir "T";
#	if ($uri ~ ^/en/(catalogsearch|customer/account/login)){
#	    set $redir "F";
#	}
#
#    if ($scheme != "https") {
#        set $redir "${redir}T";
#    }
#
#    if ($redir = "TT") {
#        rewrite ^ https://$host$uri permanent;
#    }

	# access_log /usr/local/var/log/nginx.spree.dev-to_varnish-access.log;
	# error_log /usr/local/var/log/nginx.spree.dev-to_varnish-error.log;
	access_log /usr/local/var/log/nginx.access.log;
	error_log /usr/local/var/log/nginx.error.log;

	#### Redirects Include ####
	include custom.rewrites.conf;
	include custom.redirects.conf;
	#include spree/custom.brand-rewrites.conf;


	location / {
		proxy_pass  http://varnish;

		proxy_http_version 1.1;
		proxy_set_header Connection "";

		### Set headers ####
		proxy_set_header        Accept-Encoding   "";
		proxy_set_header        Host            $host;
		proxy_set_header        X-Real-IP       $remote_addr;
		proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
		# proxy_set_header        X-mothership $geonaspers;

		### Most PHP, Python, Rails, Java App can use this header ###
		#proxy_set_header X-Forwarded-Proto https;##
		#This is better##
		proxy_set_header        X-Forwarded-Proto $scheme;
		proxy_redirect          off;
	}
}

server {
  rewrite_log on;
	listen 443 ssl http2; #http2
	server_name spree.dev;


	ssl on;
	ssl_certificate certs/nginx.crt;
	ssl_certificate_key certs/nginx.key;
	ssl_verify_depth 3;

	# location /swagger-ui {
	# 	root /var/www/spreeza;
	# 	autoindex on;
	# }
  location ^~ /en/catalogue/thumbnail/ {
      proxy_set_header Host "www.spree.co.za";
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      proxy_pass https://spreecoza;
  }
  
  location ~* /media/.*\.(jpe?g|png|gif)$ {
      proxy_set_header Host "www.spree.co.za";
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      proxy_pass https://spreecoza;
  }

	location / {
		proxy_pass              http://varnish;
        proxy_http_version      1.1;
        proxy_set_header        Connection "";
        proxy_set_header        Accept-Encoding   "";
        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;
        proxy_set_header        HTTPS on;
	}

	location ^~ /index.php/zendesk/api/ {
		rewrite ^/index.php/zendesk/api/(.*)$ https://$server_name/en/zendesk/api/$1? permanent;
	}
}

server {
  rewrite_log on;

  # Ports
  listen 8081;

  # Server details
  server_name spree.dev www.spree.dev;
  root /Users/mark.holtshausen/SPREE/environment/services/mage/htdocs;


  # Logging
  # access_log /tmp/s.dev--access.log;
  # error_log /tmp/s.dev--error.log notice;

  index index.html index.htm index.php;

  set $my_http "http";
  set $my_ssl "off";
  set $my_port "80";

  if ($http_x_forwarded_proto = "https") {
    set $my_http "https";
    set $my_ssl "on";
    set $my_port "443";
  }

  location / {
     #If you dont need rewrites then replace @rewrites with /index.php
     try_files $uri $uri/ @rewrites;
  }

  # location = /index.php/admin/admin/ {
  #   rewrite ^/index.php/admin/admin/$ https://spree.dev/admin/; 
  # }

  location @rewrites {
     # Custom rewrite rules e.g
     # rewrite ^/~(.*)/(.*)/? /users/$1/$2 last;
     # If nothing matches, send it to /index.php
     rewrite ^ /index.php last;
  }

  # This block will catch static file requests, such as images, css, js
  # The ?: prefix is a 'non-capturing' mark, meaning we do not require
  # the pattern to be captured into $1 which should help improve performance
  location ~* \.(?:ico|gif|jpe?g|png|ttf|woff|otf)$ {
    # Some basic cache-control for static files to be sent to the browser
         expires max;
         add_header Pragma public;
         add_header Cache-Control "public, must-revalidate, proxy-revalidate";
  }

  location ^~ /media/js/ {
    access_log off;
    expires 5d;
   }

  location ^~ /media/css/ {
    expires 5d;
    access_log off;
  }

  location ^~ /skin/ {
    access_log off;
  }

  location ^~ /images/ {
    access_log off;
  }

  location ^~ /media/ {
    access_log off;
  }

  location ^~ /js/ {
    access_log off;
  }

  # These locations would be hidden by .htaccess normally
  location /app/                { deny all; }
  location /includes/           { deny all; }
  location /lib/                { deny all; }
  location /media/downloadable/ { deny all; }
  location /pkginfo/            { deny all; }
  location /report/config.xml   { deny all; }
  location /var/                { deny all; }
  location /stationary/         { autoindex on; }

  location /var/export/ {
        auth_basic        "Restricted Access";
        auth_basic_user_file /usr/localhtpassword;
        autoindex       on;
  }

  # Remove the robots line if you want to use wordpress' virtual robots.txt
  location = /robots.txt  { access_log off; log_not_found off; }
  location = /favicon.ico { access_log off; log_not_found off; }

  # This prevents (dot)files from being served
  location ~ /\.          { access_log off; log_not_found off; deny all; }

  # Deny all of the following
  location ~* \.(engine|inc|info|install|module|profile|po|sh|.*sql|theme|tpl(\.php)?|xtmpl)$|^(code-style\.pl|Entries.*|Repository|Root|Tag|Template)$ {
     deny all;
     access_log off;
     log_not_found off;
  }


  location ~ .php$ {
     ## Catch 404s that try_files miss
     if (!-e $request_filename) {
        rewrite / /index.php last;
     }

     expires        off; ## Do not cache dynamic content
     fastcgi_param  MAGE_RUN_CODE en; ## Store code is defined in administration > Configuration > Manage Stores
     fastcgi_param  MAGE_RUN_TYPE store;
     fastcgi_param  MAGE_IS_DEVELOPER_MODE true;
     fastcgi_param  HTTPS $my_ssl;

     fastcgi_buffer_size 128k;
     fastcgi_buffers 4 256k;
     fastcgi_busy_buffers_size 256k;

     fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
     # fastcgi_pass unix:/var/web/php5-cgi.sock;
     fastcgi_pass 127.0.0.1:9000;
     fastcgi_keep_conn on;
     include       fastcgi_params;
    }

}
